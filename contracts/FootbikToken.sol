pragma solidity ^0.4.18;

import "./StandardToken.sol";
import "./PausableToken.sol";


contract FootbikToken is StandardToken, PausableToken {
    string public constant name = "FootbikToken";
    string public constant symbol = "FTK"; // FBT
    uint8 public constant decimals = 4;
    uint256 public INITIAL_SUPPLY = 60000000 * 10000;

    function FootbikToken() public {
        totalSupply = INITIAL_SUPPLY;
        balances[msg.sender] = INITIAL_SUPPLY;
    }
}
