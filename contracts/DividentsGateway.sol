pragma solidity ^0.4.18;

import "./Ownable.sol";
import "./FootbikToken.sol";
import "./FootbikCoin.sol";

/**
 * Class is responsible for calculating dividents in FootbikCoin for 
 * FootbikToken ownsers.
 * 
 * Scenario: 
 *  1 Prerequisites:
 *   1.1 Ownser needs to be sure DividentsGateway has enough FootbikCoin.
 *       To send coins please use FootbikCoin.transfer method to send money. 
 *       Address in this case is DividentsGateway contract address.
 *       If owner wants to send FootbikCoin back - there is special method 
 *       to do it - DividentsGateway.transferCoinsToOwner.
 * 
 *  2. Calcualting and assiging dividents: 
 *      Only owner has ability to intial this procedure. Needs to call 
 *      DividentsGateway.calculateDividends, please refer method docstring 
 *      for more info.
 *      
 *  3. Each FootbikToken owner should call DividentsGateway.getDividents 
 *     to get dividents in FootbikCoin. 
 * 
 * Example: 
 * Step #1: Two investors have 1000 and 12000 FootbikToken.
 * Step #:. Owner calls DividentsGateway.calculateDividends(315) - that means 3.15% 
 *          of dividents.
 * Step #3: First investor calls DividentsGateway.getDividents() 
 *          and receives 31.5 FootbikCoins
 * Step #4: Second investor calls DividentsGateway.getDividents() 
 *          and receives 378 FootbikCoins
 * 
 */
contract DividentsGateway is Ownable {
    
    FootbikToken public footbikToken;
    FootbikCoin public footbikCoin;
    mapping(address => uint256) public dividents;
    
    function getCoins() public onlyOwner {
        footbikToken = FootbikToken(0x46a9cC8cd7640E72aB2534a61B172E8c2DA5eA38);
        footbikCoin = FootbikCoin(0xc9eD86541942f680983031C278b577E4e2a4E93F);
    }
    
    function transferCoinsToOwner(uint256 amount) public onlyOwner {
        footbikCoin.transfer(owner, amount);
    }
    
    /**
     * Calculates and set dividents in FootbikCoins for FootbikTokens ownsers.
     * 
     *  @param percentage - dividents in FootbikCoins compare to 
     *  amount of FootbikTokens available for eahc investor. 
     *  NOTE: dividents should be specified with two additional digits.
     *  For instance 
     *   if dividents percentage is 3.25% - than parameter value should be 325
     *   if dividents percentage is 2% - than parameter value should be 200.
     * We have to use such approach as solidity doesn't support float values. 
     */
    function calculateDividends(uint256 percentage) public onlyOwner {
        footbikToken.calculateDividends(percentage);
    }
    
    function getDividents() public{
        require(msg.sender != address(0));
        uint256 dividentsAmount = footbikToken.getDividentsAmount(msg.sender);
        if (dividentsAmount > 0){
           footbikCoin.transfer(msg.sender, dividentsAmount); 
        }
        footbikToken.clearDividents(msg.sender);
    }
    
}