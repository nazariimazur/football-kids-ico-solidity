pragma solidity ^0.4.18;

import "./FootbikCrowdsale.sol";
import "./Ownable.sol";

contract Deployer is Ownable {

    FootbikCrowdsale public footbikCrowdsale;
    uint256 public constant TOKEN_DECIMALS_MULTIPLIER = 10000;
    address public multisig = 0xF84705185D8c965472Bf1372704Cf1f45Ca25bBe  ;

    function deployCrowdsale() public onlyOwner {
        footbikCrowdsale = new FootbikCrowdsale(
            1536796800, //Pre ICO Start: 13 Sep 2018 at 00:00 am GMT+3
            1537055990, //Pre ICO End: 15 Sep 2018 at 23:59 pm GMT+
            10000000 * TOKEN_DECIMALS_MULTIPLIER, //Pre ICO hardcap
            1537056000, // ICO Start: 16 Sep 2018 at 00:00 am GMT+3
            1539388790, // ICO End: 12 Oct 2018 at 23:59 pm GMT+3
            30000000 * TOKEN_DECIMALS_MULTIPLIER,  // ICO hardcap
            500000 * TOKEN_DECIMALS_MULTIPLIER, // Overal crowdsale softcap
            2222, // 1 ETH = 2222 FTOK
            multisig
        );
    }
  
    function setOwner() public onlyOwner {
        footbikCrowdsale.transferOwnership(owner);
    }
}

