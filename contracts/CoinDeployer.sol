pragma solidity ^0.4.18;

import "./Ownable.sol";
import "./FootbikCoin.sol";

contract CoinDeployer is Ownable {
    
    FootbikCoin public footbikCoin;
    
    function deployCoins() public onlyOwner {
        footbikCoin = new FootbikCoin();
        footbikCoin.transfer(owner, 100000000 * 10000);
    }
    
}