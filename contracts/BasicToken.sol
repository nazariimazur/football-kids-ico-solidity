pragma solidity ^0.4.18;

import "./ERC20Basic.sol";
import "./SafeMath.sol";


/**
 * @title Basic token
 * @dev Basic version of StandardToken, with no allowances.
 */
contract BasicToken is ERC20Basic {
    using SafeMath for uint256;

    mapping(address => uint256) public balances;
    
    address[] public addressIndex;
    mapping(address => bool) public containerIndex;
    
    mapping(address => uint256) public dividents;
    
    /**
    * @dev transfer token for a specified address
    * @param _to The address to transfer to.
    * @param _value The amount to be transferred.
    */
    function transfer(address _to, uint256 _value) public returns (bool) {
        require(_to != address(0));
        require(_value <= balances[msg.sender]);

        noteAddress(_to);
        
        // SafeMath.sub will throw if there is not enough balance.
        balances[msg.sender] = balances[msg.sender].sub(_value);
        balances[_to] = balances[_to].add(_value);
        Transfer(msg.sender, _to, _value);
        return true;
    }
    
    /**
    * We need to same each new address to have ability 
    * to send dividents to investors.
    */
    function noteAddress(address _to)  returns (bool) {
        if(containerIndex[_to] == false){
            addressIndex.push(_to);
            containerIndex[_to] = true;
        }
        
        return true;
    }
    
    /**
     * Calculates and set dividents in FootbikCoins for FootbikTokens ownsers.
     * 
     *  @param percentage - dividents in FootbikCoins compare to 
     *  amount of FootbikTokens available for eahc investor. 
     *  NOTE: dividents should be specified with two additional digits.
     *  For instance 
     *   if dividents percentage is 3.25% - than parameter value should be 325
     *   if dividents percentage is 2% - than parameter value should be 200.
     * We have to use such approach as solidity doesn't support float values. 
     */
    function calculateDividends(uint256 percentage){
        uint256 floatDigits = 100;
        require(percentage < 100 * floatDigits);
        require(percentage > 0);
        
        for (uint i = 0; i < addressIndex.length; i++) {
            dividents[addressIndex[i]] += 
                (balances[addressIndex[i]] * percentage)  / (100 * floatDigits) ;
        }
    }
    
    /**
     * @return amount of dividents in FootbikCoins for particular investor.
     */
    function getDividentsAmount(address investor) returns (uint256){
        return dividents[investor];
    }
    
    /**
     * Clears dividents for particular investor.
     * NOTE: Please make sure dividends are transfered to investor before 
     * using this function.
     */
    function clearDividents(address investor){
        dividents[investor] = 0;
    }

    /**
    * @dev Gets the balance of the specified address.
    * @param _owner The address to query the the balance of.
    * @return An uint256 representing the amount owned by the passed address.
    */
    function balanceOf(address _owner) public constant returns (uint256 balance) {
        return balances[_owner];
    }
}
