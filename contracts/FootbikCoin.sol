pragma solidity ^0.4.18;

import "./StandardToken.sol";


contract FootbikCoin is StandardToken {
    string public constant name = "FootbikCoin";
    string public constant symbol = "FCOIN";
    uint8 public constant decimals = 4;
    uint256 public INITIAL_SUPPLY = 100000000 * 10000;

    function FootbikCoin() public {
        totalSupply = INITIAL_SUPPLY;
        balances[msg.sender] = INITIAL_SUPPLY;
    }
}
